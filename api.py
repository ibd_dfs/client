import os.path
from os import makedirs
from shutil import rmtree
from typing import Optional, List, Tuple

import requests as r

from dataclasses import (
    FileInfo,
)
from exceptions import ApiException


def split_path_name(path):
    return map(Api.normalize_path, os.path.split(path))


class Api:
    def __init__(self, host, username):
        self.user = username
        self.host = host
        self.local_storage = f"./storage/{self.user}"

    def make_local_storage(self, init=False):
        if init and os.path.exists(self.local_storage):
            rmtree(self.local_storage)
        makedirs(self.local_storage, exist_ok=True)

    @staticmethod
    def normalize_path(s: str) -> str:
        s = s.strip()
        s = s.rstrip("/") if s != "/" else s
        s = os.path.normpath(s)
        s = s.lstrip("/")
        return s if s != "." else ""

    def _send_request(
            self,
            endpoint,
            method,
            data: Optional[dict] = None,
    ) -> dict:
        if data is not None and type(data) != dict:
            raise ValueError(f"data must be None or dict, got: {data}")

        params = {"user": self.user}
        body = {}

        if method.upper() == "POST":
            body = data
        elif data is not None:
            params.update(data)

        try:
            response = r.request(
                method,
                self.host + endpoint,
                data=body,
                params=params,
            )
        except r.exceptions.ConnectionError as e:
            raise ApiException([
                f"Nameserver at {self.host} is inaccessible."
            ])

        return self._check_result(response)

    def _check_result(
            self,
            response: r.Response,
    ) -> dict:
        body = response.json()

        if response.ok:
            return body
        else:
            raise ApiException(body)

    def init_storage(self) -> int:
        """
        Initialize empty storage

        :raises ApiException

        :return: size of the storage in bytes
        """
        endpoint = "/user/init"
        json = self._send_request(endpoint, method="POST", )

        self.make_local_storage(init=True)
        space_available = int(json["space_size"])
        return space_available

    def file_create(self, path: str):
        """
        Create empty file

        :raises ApiException

        :param path: path to the file
        """
        path = Api.normalize_path(path)
        directory, filename = os.path.split(path)

        endpoint = "/file/create"
        payload = {
            "path": directory,
            "name": filename,
        }

        self._send_request(endpoint, method="POST", data=payload, )

    def _file_read_request(self, path: str) -> Tuple[str, int]:
        """
        send download request to name server

        :param path: file path
        :return: address of storage node
        """
        endpoint = "/file/read"
        payload = {
            "path": path
        }

        data = self._send_request(
            endpoint,
            method="GET",
            data=payload,
        )

        return data["address"], int(data["size"])

    def _storage_node_download(self, path: str, storage_server_address: str, filesize: int) -> str:
        """
        Download a file from storage server and send locally
        :param path: logical path
        :param storage_server_address: address of the storage server
        :param filesize: size of file in bytes
        :return: local path
        """
        try:
            response = r.request(
                "GET",
                f"{storage_server_address}/file/download",
                params={
                    "full_path": f"{self.user}/{path}",
                },
                stream=True
            )
        except r.exceptions.ConnectionError:
            raise ApiException([
                f"Storage server {storage_server_address} is inaccessible."
            ])
        directory = os.path.dirname(path)
        local_directory = f"{self.local_storage}/{directory}"
        makedirs(local_directory, exist_ok=True)
        local_path = f"{self.local_storage}/{path}"
        try:
            f = open(local_path, 'wb')
        except PermissionError:
            raise ApiException([
                f"Not enough permissions to write to {local_path}."
            ])
        else:
            with f:
                for chunk in response.iter_content(chunk_size=128):
                    f.write(chunk)

            return local_path

    def file_read(self, path: str):
        """
        download a file from remote

        :raises ApiException

        :param path: path to file to download
        :return:
        """
        path = Api.normalize_path(path)
        storage_node_address, size = self._file_read_request(path)
        saved = self._storage_node_download(path, storage_node_address, size)

        return saved

    def _file_write_request(self, file_size: int, logical_path: str) -> str:
        """
        Request to nameserver where to write a file

        :param file_size: file size in bytes
        :param logical_path: path where to load
        :return: ip of storage node
        """
        endpoint = "/file/write"
        context, file_name = split_path_name(logical_path)
        payload = {
            "path": context,
            "name": file_name,
            "size": file_size,
        }

        data = self._send_request(
            endpoint,
            method="POST",
            data=payload,
        )

        return data["address"]

    def _file_write_storage_server(self, storage_server_address: str, file, path: str):
        try:
            response = r.request(
                "POST",
                f"{storage_server_address}/file/upload/",
                files={
                    "file": file
                },
                data={
                    "full_path": f"{self.user}/{path}"
                }
            )

            if not response.ok:
                raise ApiException(["Storage server error"])
        except r.exceptions.ConnectionError:
            raise ApiException([
                f"Storage server {storage_server_address} is inaccessible."
            ])

        return True

    def file_write(self, local_path, logical_path):
        """
        upload a file <local_path> to remote <logical_path>

        :raises ApiException

        :param local_path:
        :param logical_path:
        :return:
        """
        # local_path = os.path.normpath(local_path)
        logical_path = Api.normalize_path(logical_path)
        if not os.path.isfile(local_path):
            raise ApiException([f"File {local_path} does not exists", ])

        try:
            local_file = open(local_path, 'rb')
        except PermissionError:
            raise ApiException([f"Not enough permissions to open {local_path}."])
        else:
            with local_file:
                size = os.fstat(local_file.fileno()).st_size

                storage_server_address = self._file_write_request(size, logical_path)
                self._file_write_storage_server(storage_server_address, local_file, logical_path)

    def file_delete(self, path: str):
        """
        delete a file from remote

        :raises ApiException

        :param path: file_path
        :return:
        """
        path = Api.normalize_path(path)
        endpoint = "/file/delete"
        payload = {
            "path": path,
        }

        self._send_request(
            endpoint,
            method="DELETE",
            data=payload,
        )

    def get_file_info(self, path: str) -> FileInfo:
        """
        Get information about the file

        :raises ApiException

        :param path:
        :return:
        """
        path = Api.normalize_path(path)
        endpoint = "/file/info"
        payload = {
            "path": path,
        }

        data = self._send_request(
            endpoint,
            method="GET",
            data=payload,
        )

        return FileInfo(
            path=path,
            created_at=data["created_at"],
            updated_at=data["updated_at"],
            size=data["size"],
            is_directory=data["is_directory"],
        )

    def file_move(self, path_from: str, path_to: str, copy=False):
        """
        move or copy a file from one directory to another

        :raises ApiException

        :param path_from:
        :param path_to:
        :param copy: if True - copy, otherwise move
        :return:
        """
        path_from = Api.normalize_path(path_from)
        path_to = Api.normalize_path(path_to)
        endpoint = "/file/copy" if copy else "/file/move"
        payload = {
            "path_from": path_from,
            "path_to": path_to,
        }

        self._send_request(
            endpoint,
            method="POST",
            data=payload,
        )

    def list_files(self, path) -> List[FileInfo]:
        """
        List files in a specified directory

        :raises ApiException

        :param path: path to directory
        :return: list of file information
        """
        path = Api.normalize_path(path)
        endpoint = "/dir/ls"
        payload = {
            "path": path,
        }

        data = self._send_request(
            endpoint,
            method="GET",
            data=payload,
        )

        files = []
        for file in data:
            files.append(
                FileInfo(
                    path=f"{path}/{file['name']}",
                    created_at=file["created_at"],
                    updated_at=file["updated_at"],
                    is_directory=file["is_directory"],
                    size=file["size"],
                )
            )
        return files

    def make_directory(self, path):
        """
        Create d directory at path

        :raises ApiException

        :param path:
        :return:
        """
        path = Api.normalize_path(path)
        endpoint = "/dir/mk"
        context, directory_name = map(Api.normalize_path, os.path.split(path))
        payload = {
            "path": context,
            "dir_name": directory_name,
        }

        self._send_request(
            endpoint,
            method="POST",
            data=payload,
        )
