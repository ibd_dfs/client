from os.path import basename
from datetime import datetime, timezone


def utc_to_local(utc_dt):
    return utc_dt.replace(tzinfo=timezone.utc).astimezone(tz=None)


class FileInfo:
    time_format = "%Y-%m-%dT%H:%M:%S.%f%z"

    def __init__(
            self,
            path,
            created_at,
            updated_at,
            size,
            is_directory,
    ):
        self.path = path
        self.created_at = utc_to_local(datetime.strptime(created_at, FileInfo.time_format))
        self.updated_at = utc_to_local(datetime.strptime(updated_at, FileInfo.time_format))
        self.size = int(size) if size is not None else 0
        self.is_directory = bool(is_directory)

    @property
    def name(self):
        return basename(self.path)


class ClientState:
    def __init__(self, user, cwd, close):
        self.directory = cwd
        self.user = user
        self.close = close
