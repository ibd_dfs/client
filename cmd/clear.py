import argparse

from cmd.base import BaseCmd


class Clear(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("clear", description="Clear the shell")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        print("\n" * 100)
