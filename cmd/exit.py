import argparse

from cmd.base import BaseCmd


class Exit(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("exit", description="Exit the client")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        state.close = True
        return state
