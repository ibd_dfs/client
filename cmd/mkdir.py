import argparse
import os

from cmd.base import BaseCmd


class Mkdir(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("mkdir", description="Make a directory")
        parser.add_argument("path", help="Directory path")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        arguments = cls.parse_args(args)

        path = os.path.join(state.directory, arguments.path)
        api.make_directory(path)
