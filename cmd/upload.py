import argparse
import os

from cmd.base import BaseCmd


class Upload(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("upload", description="Upload a file to remote storage")
        parser.add_argument("local_path", help="File local path")
        parser.add_argument("remote_path", help="Path in dfs")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        arguments = cls.parse_args(args)

        remote_path = os.path.join(state.directory, arguments.remote_path)
        api.file_write(arguments.local_path, remote_path)
