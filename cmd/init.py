import argparse

from cmd.base import BaseCmd


class Init(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("init", description="Initialize a new storage")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        cls.parse_args(args)
        print("Are you sure, you want to initialize new storage?")
        print("This will remove all existing files at remote and local repository")
        agree = input("[Y/N]: ")
        if agree.lower() == "y":
            state.directory = "/"
            space_available = api.init_storage()
            print(f"Storage initialized, available space: {space_available} bytes")
        return state
