import argparse
import os

from cmd.base import BaseCmd


class RM(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("rm", description="Remove specified path")
        parser.add_argument("path", help="A path to file/directory to be removed")

        return parser

    @classmethod
    def run(cls, api, state, *args):
        arguments = cls.parse_args(args)
        path = os.path.join(state.directory, arguments.path)

        api.file_delete(path)
