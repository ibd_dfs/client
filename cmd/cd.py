import argparse
import os

from cmd.base import BaseCmd


class CD(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("cd", description="Change current directory")
        parser.add_argument("path", default="/", nargs="?", help="Directory to list files")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        arguments = cls.parse_args(args)

        proposed_path = os.path.join(state.directory, arguments.path)
        api.list_files(proposed_path)  # check that proposed path exists
        state.directory = proposed_path
        return state
