from abc import ABC, abstractmethod


class BaseCmd(ABC):
    @classmethod
    @abstractmethod
    def get_parser(cls):
        raise NotImplemented()

    @classmethod
    def get_help(cls):
        parser = cls.get_parser()
        return parser.prog, parser.description

    @classmethod
    def parse_args(cls, args):
        parser = cls.get_parser()
        return parser.parse_args(args)

    @classmethod
    @abstractmethod
    def run(cls, api, state, *args):
        """
        Each command should accept

        api: api.Api instance
        current_state: dataclasses.ClientState instance
        *args: arguments passed to the command

        If a command changes the state
        it must return a new state
        otherwise -> None
        """
        raise NotImplemented()
