import argparse
import os

from cmd.base import BaseCmd
from interface_utils import draw_directory


class LS(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("ls", description="List files in directory")
        parser.add_argument("path", default="", nargs="?", help="Directory to list files")
        parser.add_argument(
            "-l",
            dest="full_report",
            required=False,
            default=False,
            help="List file info",
            action="store_true"
        )
        return parser

    @classmethod
    def run(cls, api, state, *args):
        arguments = cls.parse_args(args)
        path = os.path.join(state.directory, arguments.path)

        files = api.list_files(path)
        draw_directory(files, arguments.full_report)
