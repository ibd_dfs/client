import argparse
import os

from cmd.base import BaseCmd


class Touch(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("touch", description="Create an empty file")
        parser.add_argument("path", help="A path to a new file")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        arguments = cls.parse_args(args)

        path = os.path.join(state.directory, arguments.path)
        api.file_create(path)
        print(f"File {path}, created")
