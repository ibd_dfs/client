import argparse
import os

from cmd.base import BaseCmd
from interface_utils import draw_file_info


class File(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("file", description="Get info about file")
        parser.add_argument("path", help="File path")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        arguments = cls.parse_args(args)
        path = os.path.join(state.directory, arguments.path)

        file_info = api.get_file_info(path)
        draw_file_info(file_info)
