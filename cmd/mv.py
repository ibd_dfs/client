import argparse
import os

from cmd.base import BaseCmd


class MV(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("mv", description="Move a file")
        parser.add_argument("src", help="Source file path")
        parser.add_argument("dest", help="Destination file path")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        arguments = cls.parse_args(args)

        path_src = os.path.join(state.directory, arguments.src)
        path_dest = os.path.join(state.directory, arguments.dest)

        api.file_move(path_src, path_dest, copy=False)
