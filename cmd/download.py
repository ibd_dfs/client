import argparse
import os

from cmd.base import BaseCmd


class Download(BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("download", description="Download a file to localstorage")
        parser.add_argument("path", help="File path")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        arguments = cls.parse_args(args)

        path = os.path.join(state.directory, arguments.path)
        saved_to = api.file_read(path)
        print(f"A file {path} saved to {saved_to}.")
