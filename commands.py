import argparse

from dataclasses import ClientState
from exceptions import ApiException
import cmd
from interface_utils import print_help


def clear_cmd():
    """
    Clear command to be arbitrary used
    """
    cmd.Clear.run(None, None)


COMMANDS = [
    cmd.CD,
    cmd.Clear,
    cmd.CP,
    cmd.Download,
    cmd.Exit,
    cmd.File,
    cmd.Init,
    cmd.LS,
    cmd.Mkdir,
    cmd.MV,
    cmd.RM,
    cmd.Touch,
    cmd.Upload,
]
HELP_MSG = [command.get_help() for command in COMMANDS]


class Help(cmd.BaseCmd):
    @classmethod
    def get_parser(cls):
        parser = argparse.ArgumentParser("help", description="Show help message")
        return parser

    @classmethod
    def run(cls, api, state, *args):
        print_help(HELP_MSG)


COMMANDS.append(Help)
PROGRAM_TO_CLASS = dict([(command.get_help()[0], command) for command in COMMANDS])


def parse_command(api, state: ClientState, user_input):
    tokens = list(filter(bool, user_input.split(" ")))
    command, arguments = tokens[0], tokens[1:]

    try:
        if command in PROGRAM_TO_CLASS:
            handler = PROGRAM_TO_CLASS[command]
            new_state = handler.run(api, state, *arguments)
            if new_state is not None:
                # if a program didn't return state
                # consider the state to be the same
                state = new_state
        else:
            print(f"{command}: command unknown. Type help to get available commands.")
    except ApiException as e:
        print(e)
    except SystemExit:
        pass

    return state
