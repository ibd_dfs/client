from typing import List


class ApiException(Exception):
    def __init__(self, messages: List[str]):
        super(ApiException, self).__init__(
            messages[0]
        )
