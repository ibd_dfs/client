FROM python:3.8-alpine

ENV nameserver_address http://localhost:3559
ENV username user

WORKDIR /app
COPY requirements.txt ./
COPY cmd/* cmd/
COPY ./*.py ./

RUN pip3 install -r requirements.txt
CMD python3 main.py ${nameserver_address} ${username}