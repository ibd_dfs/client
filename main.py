import argparse
import readline  # noqa

from api import Api
from commands import parse_command, clear_cmd
from dataclasses import ClientState
from interface_utils import print_cwd


def get_args() -> (str, str):
    parser = argparse.ArgumentParser(
        description="a client for a dfs."
    )
    parser.add_argument("ip", help="IP address of the nameserver")
    parser.add_argument("username", help="Username")
    args = parser.parse_args()
    return args.ip, args.username


def start_app(api, username):
    state = ClientState(cwd="/", user=username, close=False)
    clear_cmd()
    while not state.close:
        print_cwd(state)
        try:
            cmd = input()
        except EOFError:
            return
        if cmd:
            state = parse_command(api, state, cmd)


if __name__ == '__main__':
    ip, user = get_args()
    api_client = Api(ip, user)
    start_app(api_client, user)
