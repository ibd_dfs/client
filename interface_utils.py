import os
import sys
from typing import List, Optional

import tabulate
from termcolor import colored

from dataclasses import FileInfo, ClientState


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def get_terminal_size() -> Optional[List[int]]:
    """
    If stdin is a tty, then returns size in characters.
    Otherwise returns None
    :return: (height, width)
    """
    if sys.stdin.isatty():
        size = list(map(int, os.popen('stty size', ).read().split()))
        return size
    else:
        return None


def print_cwd(state: ClientState):
    username_colored = colored(state.user, "green", attrs=["bold"])
    path_colored = colored(state.directory, "blue", attrs=["bold"])
    print(f"{username_colored}:{path_colored}$ ", end="")


def draw_directory(files: List[FileInfo], full_report: bool):
    def file_color(f: FileInfo):
        if f.is_directory:
            return colored(f.name, "blue", attrs=["bold"])
        else:
            return f.name

    files.sort(key=lambda x: x.name)
    names_colored = [file_color(f) for f in files]
    if full_report:
        report = [
            ["size", "created_at", "updated_at", "name"],
        ]
        for i, file in enumerate(files):
            report.append([
                file.size,
                file.created_at,
                file.updated_at,
                names_colored[i],
            ])
        print(f"Total: {len(files)}")
        print(tabulate.tabulate(report, headers="firstrow"))

    else:
        terminal_size = get_terminal_size()
        names_per_line = 2
        if terminal_size is not None:
            width = terminal_size[1]
            max_length = len(max(files, key=lambda x: x.name).name)
            names_per_line = width // max_length

        names_split = list(chunks(names_colored, names_per_line))

        print(tabulate.tabulate(names_split, tablefmt="plain"))


def draw_file_info(info: FileInfo):
    table = [
        ["Name: ", info.name, ],
        ["Size: ", info.size, ],
        ["Created at: ", info.created_at, ],
        ["Updated at: ", info.updated_at, ],
        ["Is directory: ", info.is_directory, ],
    ]
    print(tabulate.tabulate(table, tablefmt="plain"))


def print_help(help_arr):
    print(tabulate.tabulate(help_arr))
