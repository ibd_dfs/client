#!/usr/bin/env bash
docker run -i \
    -v "$(pwd)/storage":/app/storage \
    -v upload:/upload \
    winnerokay/dfs_client:latest \
    python main.py "$1" "$2" < "/dev/stdin"
sudo chown -R "$USER" "$(pwd)/storage"